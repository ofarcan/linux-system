Bir Nginx container ayağa kaldırıldı. Yapılan işlemler şu şekilde. default.conf dosyası ve dockerfile aynı dizine koyulur.

- Nginx'in global'i de kullanılabilir ancak burada linuxserver/nginx kullanıldı.  dockerfile içeriği şu şekilde.

  ```
  FROM linuxserver/nginx
  COPY default.conf /etc/nginx/conf.d/default.conf
  
  ```

- Aynı dizinde bulunan default.conf dosyası içeriği şu şekilde:

  ```bash
  server {
      listen 80;
      server_name www.example.com;
  
      location / {
          proxy_pass http://www.example.com:4200;
      }
  
      location /api {
          proxy_pass http://www.example.com:20201/swagger-ui.html;
      }
  }
  
  ```

- `docker build -t nginx-example .` komutu ile dockerfile tetiklenir.

  ```bash
  omrfarukcan@omer-pc ~> docker build -t nginx-example .
  Sending build context to Docker daemon  3.072kB
  Step 1/2 : FROM linuxserver/nginx
   ---> b5b7a777f612
  Step 2/2 : COPY default.conf /etc/nginx/conf.d/default.conf
   ---> Using cache
   ---> 7b0e4b2a3efb
  Successfully built 7b0e4b2a3efb
  Successfully tagged nginx-example:latest
  ```

- `docker run -ti -p 80:80 -p 443:443 -p 4200:4200 --name nginxx nginx-example` komutu ile ngnixx ismine sahip, 80,443 ve 4200 portları açık olan bir konteyner koşturulur. 

  ```bash
  omrfarukcan@omer-pc ~> docker ps -a
  CONTAINER ID   IMAGE            COMMAND                  CREATED        STATUS                    PORTS                                                                                                                 NAMES
  4d617da2bfc0   nginx-example1   "/init"                  18 hours ago   Up 18 hours               0.0.0.0:80->80/tcp, :::80->80/tcp, 0.0.0.0:443->443/tcp, :::443->443/tcp, 0.0.0.0:4200->4200/tcp, :::4200->4200/tcp   nginxx
  
  ```

- Docker içerisinde default.conf dosyası şu şekilde düzenlenmiş olur(bu işlem dockerfile tetiklendiğinde gerçekleştirilmiş olur.).

  ```
  root@4d617da2bfc0:/# cat /etc/nginx/conf.d/default.conf 
  server {
      listen 80;
      server_name www.example.com;
  
      location / {
          proxy_pass http://www.example.com:4200;
      }
  
      location /api {
          proxy_pass http://www.example.com:20201/swagger-ui.html;
      }
  }
  ```

  

* Konteyner içerisindeki `/etc/hosts` dosyası içeriği şu şekilde. Burada görünen 10.1.204.*** ipsi example.com'un bulunacağı cihazı gösterir.

  ```bash
  root@4d617da2bfc0:/# cat /etc/hosts
  127.0.0.1	localhost
  10.1.204.***	example.com www.example.com #Buradaki ip bilgisi example.com'un nerede olacağını gösterir.
  
  ::1	localhost ip6-localhost ip6-loopback
  fe00::0	ip6-localnet
  ff00::0	ip6-mcastprefix
  ff02::1	ip6-allnodes
  ff02::2	ip6-allrouters
  172.17.0.2	4d617da2bfc0
  ```

* Bu düzenlemelerden sonra `nginx -s reload` ile konfigürasyon ayarları apply edilmiş olur.

* Local makinede `/etc/hosts` içeriği şu şekilde düzenlenir.

  ```
  omrfarukcan@omer-pc ~> cat /etc/hosts | grep 127.0.0.1
  127.0.0.1	localhost example.com www.example.com 
  ```

* Burdaki durumda konteynerin ayağa kaldırıldığı herhangi bir bilgisayarda example.com'a gidildiğinde 80 portuna yönlendirilir ve bu port docker container tarafından dinlendiği için istek docker içerisine gider.

* Gelen istek Docker container içerisinde ise nginx konfigürasyonunda 80 portunu kontrol ettiğinde `wwww.example.com` 'a gitmiş olur.

  * `example.com/api` olarak istek atıldığında ise nginx konfigürasyonu içerisinde `http://www.example.com:20201/swagger-ui.html;` adresine yönlendirilmiş olur.
