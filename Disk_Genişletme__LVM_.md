### Disk Genişletme (LVM)



*Bu işlemler sanal makinede yapıldı ancak fiziksel ortamda da kullanılabilir.*

```bash
balistika@controller:~$ df -h
Filesystem                         Size  Used Avail Use% Mounted on
udev                               3,9G     0  3,9G   0% /dev
tmpfs                              796M  1,3M  795M   1% /run
/dev/mapper/ubuntu--vg-ubuntu--lv   19G   15G  3,4G  82% /
tmpfs                              3,9G     0  3,9G   0% /dev/shm
tmpfs                              5,0M     0  5,0M   0% /run/lock
tmpfs                              3,9G     0  3,9G   0% /sys/fs/cgroup
/dev/sda2                          2,0G  106M  1,7G   6% /boot
tmpfs                              796M     0  796M   0% /run/user/1000

```



Kullanılan disk durumu gözden geçirildiğinde 3.4G kadar kullanılabilir alan olduğu görünüyor.

`lsblk` ile kontrol edildiğinde durum şu şekilde.

```bash
stack@controller:~$ lsblk
NAME                                                                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
loop0                                                                       7:0    0   30G  0 loop 
├─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool_tmeta   253:1    0   32M  0 lvm  
│ └─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool-tpool 253:3    0 28.5G  0 lvm  
│   └─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool     253:4    0 28.5G  1 lvm  
└─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool_tdata   253:2    0 28.5G  0 lvm  
  └─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool-tpool 253:3    0 28.5G  0 lvm  
    └─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool     253:4    0 28.5G  1 lvm  
sda                                                                         8:0    0   40G  0 disk 
├─sda1                                                                      8:1    0    1M  0 part 
├─sda2                                                                      8:2    0    2G  0 part /boot
└─sda3                                                                      8:3    0   38G  0 part 
  └─ubuntu--vg-ubuntu--lv                                                 253:0    0   19G  0 lvm  /
sr0                                                                        11:0    1 1024M  0 rom  
```



Burada `sda3` kısmına baktığımızda disk yapısının LVM olarak yapılandırıldığı görünmekte. Bu sebeple fdisk kullanarak genişletme yapılamıyor. `ext4` yapılandırması kullanıldığında fdisk ile bu işlem yapılabilir.

#### İşlem Adımları

- Yeni bir disk eklenir. Disk eklendikten sonra lsblk çıktısı aşağıdaki gibi görünür. 60G boyutunda yeni disk eklenmiş görünüyor. Henüz herhangi bir yere mount edilmemiş ve mevcut diske eklenmemiş durumda.

  ```
  balistika@controller:~$ lsblk
  NAME                                                                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
  loop0                                                                       7:0    0   30G  0 loop 
  ├─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool_tmeta   253:1    0   32M  0 lvm  
  │ └─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool-tpool 253:3    0 28,5G  0 lvm  
  │   └─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool     253:4    0 28,5G  1 lvm  
  └─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool_tdata   253:2    0 28,5G  0 lvm  
    └─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool-tpool 253:3    0 28,5G  0 lvm  
      └─stack--volumes--lvmdriver--1-stack--volumes--lvmdriver--1--pool     253:4    0 28,5G  1 lvm  
  sda                                                                         8:0    0   40G  0 disk 
  ├─sda1                                                                      8:1    0    1M  0 part 
  ├─sda2                                                                      8:2    0    2G  0 part /boot
  └─sda3                                                                      8:3    0   38G  0 part 
    └─ubuntu--vg-ubuntu--lv                                                 253:0    0   19G  0 lvm  /
  sdb                                                                         8:16   0   60G  0 disk 
  sr0                                                                        11:0    1 1024M  0 rom  
  
  ```

- `df -h` komutu ile kontrol edildiğinde de bir değişiklik görünmemekte.

  ```
  balistika@controller:~$ df -h
  Filesystem                         Size  Used Avail Use% Mounted on
  udev                               3,9G     0  3,9G   0% /dev
  tmpfs                              796M  1,4M  795M   1% /run
  /dev/mapper/ubuntu--vg-ubuntu--lv   19G   15G  3,3G  82% /
  tmpfs                              3,9G   20K  3,9G   1% /dev/shm
  tmpfs                              5,0M     0  5,0M   0% /run/lock
  tmpfs                              3,9G     0  3,9G   0% /sys/fs/cgroup
  /dev/sda2                          2,0G  106M  1,7G   6% /boot
  tmpfs                              796M     0  796M   0% /run/user/1000
  
  ```

  

  1. `lvm2` kurulması gerekiyor. İşlem yapılan cihazda zaten kurulu olduğundan dolayı işlem yapılmadı.

     ```bash
     root@controller:/# apt install lvm2
     Reading package lists... Done
     Building dependency tree       
     Reading state information... Done
     lvm2 is already the newest version (2.03.07-1ubuntu1).
     0 upgraded, 0 newly installed, 0 to remove and 7 not upgraded.
     ```

     

  2. sdb diski için bir fiziksel volume oluşturulur.

     ```bash
     root@controller:/# pvcreate /dev/sdb
       Physical volume "/dev/sdb" successfully created.
     ```

  3. Fiziksel volume için bir volume group oluşturulur.

     ```bash
     root@controller:/# vgextend ubuntu-vg /dev/sdb
       Volume group "ubuntu-vg" successfully extended
     ```

  4. Mevcut volume group'u genişletilir.

     ```bash
     sudo vgextend ubuntu-vg /dev/sdb
     ```

  5. Logical volume'u genişletilir.

     ```bash
     root@controller:/# lvextend -l +100%FREE /dev/mapper/ubuntu--vg-ubuntu--lv
       Size of logical volume ubuntu-vg/ubuntu-lv changed from <19,00 GiB (4863 extents) to 97,99 GiB (25086 extents).
       Logical volume ubuntu-vg/ubuntu-lv successfully resized.
     ```

  6. Logical volume'un boyutunu büyütülür.

     ```bash
     root@controller:/# sudo resize2fs /dev/mapper/ubuntu--vg-ubuntu--lv 
     resize2fs 1.45.5 (07-Jan-2020)
     Filesystem at /dev/mapper/ubuntu--vg-ubuntu--lv is mounted on /; on-line resizing required
     old_desc_blocks = 3, new_desc_blocks = 13
     The filesystem on /dev/mapper/ubuntu--vg-ubuntu--lv is now 25688064 (4k) blocks long.
     ```

- İşlemler tamamlandıktan sonra `df -h` ile disk durumu kontrol edilir.

  ```bash
  root@controller:/# df -h
  Filesystem                         Size  Used Avail Use% Mounted on
  udev                               3,9G     0  3,9G   0% /dev
  tmpfs                              796M  1,4M  795M   1% /run
  /dev/mapper/ubuntu--vg-ubuntu--lv   97G   15G   78G  16% /
  tmpfs                              3,9G   20K  3,9G   1% /dev/shm
  tmpfs                              5,0M     0  5,0M   0% /run/lock
  tmpfs                              3,9G     0  3,9G   0% /sys/fs/cgroup
  /dev/sda2                          2,0G  106M  1,7G   6% /boot
  tmpfs                              796M     0  796M   0% /run/user/1000
  ```

  